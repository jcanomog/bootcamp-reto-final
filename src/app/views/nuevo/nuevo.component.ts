import { Component, OnInit } from '@angular/core';
import { PostEmployeeRequest, PostEmployeeResponse } from 'src/app/core/interfaces/employee.interface';
import { EmployeeService } from 'src/app/core/services/employee.service';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.scss']
})
export class NuevoComponent implements OnInit {

  public testEmployee: PostEmployeeRequest = {
    name: 'Juan Manuel Cano Moguer',
    salary: '21000',
    age: '25'
  }

  public employee: PostEmployeeRequest = {
    name: '',
    salary: '',
    age: '',
  };

  public status: string = '';
  public message: string = '';

  constructor(
    private service: EmployeeService
  ) { }

  ngOnInit(): void { }

  createEmployee(employee) {
    this.service.postEmployee(employee).subscribe(
      res => {
        this.status = 'success'
        this.message = 'El empleado se creó correctamente'
      }),
      error => {
        debugger;
        this.status = 'error'
        this.message = 'Ha ocurrido un error. Vuelva a intentarlo.'
      }
  }

  submit(employee) {
    if (employee.valid) {
      this.createEmployee(employee['value']);
    }
  }

}
