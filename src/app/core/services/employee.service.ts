import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import {
  GetEmployees,
  PostEmployeeRequest,
  PostEmployeeResponse
} from '../interfaces/employee.interface';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private http: HttpClient
  ) { }

  getEmployees(): Observable<GetEmployees> {
    return this.http.get<GetEmployees>('https://dummy.restapiexample.com/api/v1/employees');
  }

  postEmployee(employee: PostEmployeeRequest): Observable<any> {
    return this.http.post<PostEmployeeResponse>('http://dummy.restapiexample.com/api/v1/create', JSON.stringify(employee));
  }
}
