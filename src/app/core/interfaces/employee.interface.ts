export interface Employee {
    employee_age: number
    employee_name: string
    employee_salary: number
    id: number
    profile_image: string
}

export interface GetEmployees {
    data: Employee[]
    message: string
    status: string
}

export interface PostEmployeeRequest {
    name: string
    salary: string
    age: string
}

export interface PostEmployeeResponse {
    status: string
    data: {
        name: string
        salary: string
        age: string
        id: number
    }
}