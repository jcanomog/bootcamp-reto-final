import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadosComponent } from './views/empleados/empleados.component';
import { HomeComponent } from './views/home/home.component';
import { NuevoComponent } from './views/nuevo/nuevo.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'empleados', component: EmpleadosComponent },
  { path: 'nuevo', component: NuevoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
